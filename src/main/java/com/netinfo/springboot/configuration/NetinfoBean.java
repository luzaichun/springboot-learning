package com.netinfo.springboot.configuration;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "netinfo")
@PropertySource(value = "classpath:netinfo.properties")//1.5.9之前不是这样设置得，，资源文件地址
@Data
public class NetinfoBean{
    private String username;
    private String password;
    private String language;
}
