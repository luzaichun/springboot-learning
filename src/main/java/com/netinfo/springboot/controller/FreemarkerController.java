package com.netinfo.springboot.controller;

import com.netinfo.springboot.common.ServerResponse;
import com.netinfo.springboot.configuration.NetinfoBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@Slf4j
@RequestMapping("/freemarker/")
public class FreemarkerController {

    @Autowired
    private NetinfoBean netinfoBean;

    @GetMapping("getBean1")
    public String getBean1(){
       log.info("。。。。。。。。。进入getBean1方法");
        return "freemarker/index";
    }

    @GetMapping("getBean2")
    public String getBean2(ModelMap map){
        log.info("。。。。。。。。。进入getBean222方法");
        map.addAttribute("data",netinfoBean);
        return "freemarker/user/user";
    }

    @GetMapping("getBean3")
    public ModelAndView getBean3(){
        log.info("。。。。。。。。。进入getBean33方法");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("freemarker/user/user");
        modelAndView.addObject("data",netinfoBean);
        return modelAndView;
    }

}
