package com.netinfo.springboot.controller;

import com.netinfo.springboot.configuration.NetinfoBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@Slf4j
@RequestMapping("/thymeleaf/")
public class TymeleafController {

    @Autowired
    private NetinfoBean netinfoBean;

    @RequestMapping("getBean1")
    public String getBean1(){
       log.info("。。。。。。。。。进入getBean1方法");
        return "thymeleaf/index";
    }

    @GetMapping("getBean2")
    public String getBean2(ModelMap map){
        log.info("。。。。。。。。。进入getBean222方法");
        map.addAttribute("data",netinfoBean);
        return "thymeleaf/user/user";
    }

    @GetMapping("getBean3")
    public ModelAndView getBean3(){
        log.info("。。。。。。。。。进入getBean33方法");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("thymeleaf/user/user");
        modelAndView.addObject("data",netinfoBean);
        return modelAndView;
    }

}
