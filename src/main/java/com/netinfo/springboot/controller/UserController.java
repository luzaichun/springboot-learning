package com.netinfo.springboot.controller;

import com.netinfo.springboot.common.ResponseCode;
import com.netinfo.springboot.common.ServerResponse;
import com.netinfo.springboot.configuration.NetinfoBean;
import com.netinfo.springboot.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;

@RestController
@RequestMapping("/user/")
@Slf4j
public class UserController {

    @Autowired
    private NetinfoBean netinfoBean;

    @RequestMapping(value = "getUser", method = RequestMethod.GET)
    public ServerResponse<User> getUser() {
        User user = new User();
        user.setId(1);
        user.setName("陆凌枫");
        user.setPassword("123321");
        user.setAge(18);
        user.setBirthday(new Date());
        return ServerResponse.createBySuccess(ResponseCode.SUCCESS.getDesc(), user);
    }

    @RequestMapping(value = "getBean", method = RequestMethod.GET)
    public ServerResponse  get(){
        log.info(netinfoBean.toString());
        NetinfoBean bean = new NetinfoBean();
        BeanUtils.copyProperties(netinfoBean,bean);//直接返回netinfoBean会报反序列化失败
        return ServerResponse.createBySuccess(bean);
    }
}
