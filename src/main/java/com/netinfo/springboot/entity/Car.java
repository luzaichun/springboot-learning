package com.netinfo.springboot.entity;

public class Car {
    private Integer carId;

    private String carName;

    private Integer carPrice;

    public Car(Integer carId, String carName, Integer carPrice) {
        this.carId = carId;
        this.carName = carName;
        this.carPrice = carPrice;
    }

    public Car() {
        super();
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName == null ? null : carName.trim();
    }

    public Integer getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(Integer carPrice) {
        this.carPrice = carPrice;
    }
}