package com.netinfo.springboot.service;

import com.netinfo.springboot.common.ServerResponse;

public interface CarService {

    ServerResponse getList();

    ServerResponse getCarById(Integer id);
}
