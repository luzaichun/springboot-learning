package com.netinfo.springboot.service.impl;

import com.netinfo.springboot.common.ServerResponse;
import com.netinfo.springboot.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

//    @Autowired
//    private CarMapper carMapper;
    @Override
    public ServerResponse getList() {
      //  List<Car> carList = carMapper.queryList();
        return ServerResponse.createBySuccess("查询car列表成功");
    }

    @Override
    public ServerResponse getCarById(Integer id) {
        //Car car = carMapper.selectByPrimaryKey(id);
        return ServerResponse.createBySuccess();
    }
}
